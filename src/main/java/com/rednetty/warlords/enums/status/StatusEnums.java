package com.rednetty.warlords.enums.status;


import org.bukkit.Material;

public enum StatusEnums {
    BLUE("&7[&b✪&7] ", "&bBLUE ", "warlords.status.blue", Material.DIAMOND),
    RED("&7[&c✘&7] ", "&cRED ", "warlords.status.red", Material.BLAZE_POWDER),
    YELLOW("&7[&e❖&7] ", "&eYELLOW ", "warlords.status.yellow", Material.GOLD_INGOT),
    GREEN("&7[&a✿&7] ", "&aGREEN ", "warlords.status.green", Material.EMERALD),
    NONE("", "&fNONE", "", Material.BARRIER);


    private final String displayName;
    private final String itemName;
    private final String perms;
    private final Material material;

    StatusEnums(String displayName, String itemName, String perms, Material material) {
        this.itemName = itemName;
        this.perms = perms;
        this.material = material;
        this.displayName = displayName;
    }

    /**
     * Display name of the Status that shows above the players head
     *
     * @return
     */
    public String getDisplayName() {

        return displayName;
    }


    /**
     * The Material displayed inside the Status Menu
     *
     * @return
     */
    public Material getMaterial() {
        return material;
    }


    /**
     * Perm required to see the Status in the Status Menu
     *
     * @return
     */
    public String getPerms() {
        return perms;
    }


    /**
     * Name of the Status in the Item Menu
     *
     * @return
     */
    public String getItemName() {
        return itemName;
    }
}
