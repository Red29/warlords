package com.rednetty.warlords.mechanics;

import com.rednetty.warlords.mechanics.combat.CombatMechanic;
import com.rednetty.warlords.mechanics.event.EventMechanic;
import com.rednetty.warlords.mechanics.menus.MenuHandler;
import com.rednetty.warlords.mechanics.status.StatusMechanic;

import java.util.stream.Stream;

public class MechanicHandler {

    private MenuHandler menuHandler;
    private StatusMechanic statusMechanic;
    private EventMechanic eventMechanic;
    private CombatMechanic combatMechanic;

    /**
     * Initializes Classes using the Mechanic system
     */
    public void init() {
        Stream.of(
                menuHandler = new MenuHandler(),
                statusMechanic = new StatusMechanic(),
                eventMechanic = new EventMechanic(),
                combatMechanic = new CombatMechanic()
        ).forEach(mechanic -> mechanic.onEnable());
    }

    /**
     * Disables all Classes using the Mechanic system
     */
    public void stop() {
        Stream.of(
                menuHandler,
                statusMechanic,
                eventMechanic,
                combatMechanic
        ).forEach(mechanic -> mechanic.onDisable());
    }


    public EventMechanic getEventMechanic() {
        return eventMechanic;
    }

    public MenuHandler getMenuHandler() {
        return menuHandler;
    }

    public StatusMechanic getStatusMechanic() {
        return statusMechanic;
    }
}
