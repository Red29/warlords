package com.rednetty.warlords.mechanics.event;

import com.rednetty.warlords.WarlordsMain;
import com.rednetty.warlords.configs.EventConfig;
import com.rednetty.warlords.enums.status.StatusEnums;
import com.rednetty.warlords.mechanics.Mechanic;
import com.rednetty.warlords.mechanics.event.arenas.Arena;
import com.rednetty.warlords.mechanics.event.arenas.ArenaHandler;
import com.rednetty.warlords.mechanics.event.gamemodes.LTSHandler;
import com.rednetty.warlords.mechanics.status.StatusMechanic;
import com.rednetty.warlords.utils.string.StringUtil;
import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class EventMechanic extends Mechanic implements Listener {

    /*Lists*/
    ArrayList<Player> playerList = new ArrayList<>();
    ArrayList<Player> spectatorList = new ArrayList<>();
    StatusMechanic statusMechanic = WarlordsMain.getMechanicHandler().getStatusMechanic();

    /*Classes*/
    private ArenaHandler arenaHandler;
    private LTSHandler LTSHandler;

    /*Constants*/
    private int timer = 0;
    private Arena activeArena;
    private GameEvent gameEvent;
    private boolean activeGame = false;
    private int originalTime = 0;
    private StatusEnums lastStatus = StatusEnums.RED;


    /**
     * Gets instance of LTSHandler class
     *
     * @return - Returns the LTSHandler class
     */
    public LTSHandler getLTSHandler() {
        return LTSHandler;
    }

    /**
     * Gets instance of Arena Handler Class
     *
     * @return Instance of ArenaHandler
     */
    public ArenaHandler getArenaHandler() {
        return arenaHandler;
    }

    @Override
    public void onEnable() {
        listener(this);

        arenaHandler = new ArenaHandler();
        arenaHandler.init();

        LTSHandler = new LTSHandler();
        listener(LTSHandler);
        EventConfig.setup();


        /*
         * This sets the game to War Gamemode for now, it's static until more are made
         * Eventually once there are more it will either rotate Gamemodes or randomly select (Yet to be decided)*/
        gameEvent = LTSHandler;

        resetTimer();
        /*Used to tick down the timer for the game*/
        new BukkitRunnable() {
            @Override
            public void run() {
                tickGame();

            }
        }.runTaskTimer(WarlordsMain.getInstance(), 20L, 20L);
    }

    @Override
    public void onDisable() {
        arenaHandler.close();
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if (getPlayerList().contains(event.getPlayer())) {
            if (!event.getMessage().toLowerCase().contains("event") && !event.getPlayer().isOp()) {
                event.getPlayer().sendMessage("&8[&6&lWarlords&8] &cYou must use /event leave before using any other commands.");
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (getSpectatorList().contains(player)) {
            player.setGameMode(GameMode.SURVIVAL);
        }
        if (getPlayerList().contains(player)) {
            if (isActiveGame()) {
                eliminatePlayer(player);
            } else {
                removePlayer(player);
            }
        } else if (getSpectatorList().contains(player)) {
            removeSpectator(player);
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (getSpectatorList().contains(player)) {
            player.setGameMode(GameMode.SURVIVAL);
        }
        if (getPlayerList().contains(player)) {
            if (isActiveGame()) {
                eliminatePlayer(player);
            } else {
                removePlayer(player);
            }
        } else if (getSpectatorList().contains(player)) {
            removeSpectator(player);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (!isActiveGame()) {
            event.getPlayer().spigot().sendMessage(getTimerMessage());
        }
        if (event.getPlayer().getGameMode() == GameMode.SPECTATOR) {
            event.getPlayer().setGameMode(GameMode.SURVIVAL);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + event.getPlayer().getName());
        }
    }

    /**
     * Deals with the pre-game countdown
     */
    private void tickGame() {
        if (isActiveGame()) return;
        if ((!(timer >= 0)) || getArenaHandler().getArenasList().isEmpty()) {
            resetTimer();
            return;
        }
        if (timer == 10 && !getArenaHandler().getArenasList().isEmpty()) {
            activeArena = getArenaHandler().getRandomArena();
        }
        sendQueueBar();
        Bukkit.getOnlinePlayers().forEach(player -> {
            if ((timer == originalTime) || timer == (originalTime / 2) || timer == (originalTime / 3) || timer == (originalTime / 4)) {
                player.spigot().sendMessage(getTimerMessage());
            } else switch (this.timer) {
                case 10:
                    player.spigot().sendMessage(getTimerMessage());
                    if (playerList.contains(player)) {
                        player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cSelecting arena.."));
                    }
                    break;
                case 8:
                    if (playerList.contains(player)) {
                        player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cThe arena has been selected! - &b" + activeArena.getArenaName()));
                    }
                    break;
                case 5:
                case 4:
                case 3:
                case 2:
                case 1:
                    player.spigot().sendMessage(getTimerMessage());
                    if (playerList.contains(player)) {
                        WarlordsMain.getTitleManagerAPI().sendTitle(player, StringUtil.colorCode("&e&l" + timer + ""), 0, 20, 0);
                    }
                    if (timer == 1) {
                        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, 1F, 1F);
                    } else {
                        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BELL, 1F, 1F);
                    }
            }
        });
        timer--;
        if (timer == 0) {
            if (getTotalPlayers() > 1) {
                startGame(gameEvent);
            } else {
                Bukkit.broadcastMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cGame timer reset due to not enough players."));
                resetTimer();
                return;
            }
        }
    }

    /**
     * the Last Status used when Choosing Teams
     *
     * @return - StatusEnums
     */
    public StatusEnums getLastStatus() {
        return lastStatus;
    }

    /**
     * Used to set the last Status
     *
     * @param lastStatus
     */
    public void setLastStatus(StatusEnums lastStatus) {
        this.lastStatus = lastStatus;
    }

    /**
     * Notifies players who have joined the queue how long is left.
     */
    public void sendQueueBar() {
        playerList.forEach(player ->
                WarlordsMain.getTitleManagerAPI().sendActionbar(player, StringUtil.colorCode("&8[&6&lWarlords&8] &cThe &8[&2" + gameEvent.getEventName() + "&8] &cevent is &cstarting in &e" + StringUtil.getDurationString(timer))));
    }

    /**
     * Updates all the lists and correctly eleminates the player (Try not to ever use removePlayer alone.
     *
     * @param player - Player to eliminate.
     */
    public void eliminatePlayer(Player player) {
        StatusMechanic statusMechanic = WarlordsMain.getMechanicHandler().getStatusMechanic();
        StatusEnums statusEnums = statusMechanic.getStatus(player);
        sendGameMessage(StringUtil.colorCode(statusEnums.getDisplayName() + player.getName() + " &7has been eliminated."));
        player.getLocation().getWorld().strikeLightningEffect(player.getLocation());
        removePlayer(player);
        player.getInventory().clear();
        sendRemainingBar();


        addSpectator(player);
        int redLeft = getTeamSize(StatusEnums.RED);
        int blueLeft = getTeamSize(StatusEnums.BLUE);
        if (blueLeft == 0 || redLeft == 0) {
            StatusEnums winners = getWinningTeam();
            Bukkit.broadcastMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &c" + winners.getDisplayName() + "&chas won the " + getGameEvent().getEventName() + " event!"));
            new BukkitRunnable() {
                @Override
                public void run() {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + player.getName());
                    endGame();
                }
            }.runTaskLater(WarlordsMain.getInstance(), 60L);
        }
    }

    /**
     * Runs the command to give the player his kit
     *
     * @param player - Player to give
     */
    public void giveKit(Player player) {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kit FFA " + player.getName());
    }


    /**
     * Displays how many players are left for the remaining alive.
     */
    public void sendRemainingBar() {
        if (!isActiveGame()) return;
        int redLeft = getTeamSize(StatusEnums.RED);
        int blueLeft = getTeamSize(StatusEnums.BLUE);
        List<Player> players = new ArrayList<>();
        getPlayerList().forEach(player -> players.add(player));
        getSpectatorList().forEach(player -> players.add(player));

        players.forEach(player -> {
            if (blueLeft == 0 || redLeft == 0) {
                WarlordsMain.getTitleManagerAPI().clearActionbar(player);
            } else {
                WarlordsMain.getTitleManagerAPI().sendActionbar(player, StringUtil.colorCode(StatusEnums.RED.getDisplayName() + "&7➤ " + redLeft + " left &f- " + StatusEnums.BLUE.getDisplayName() + "&7➤ " + blueLeft + " left"));
            }
        });
    }

    /**
     * Gets the BaseComponent message for the Timer Announcements
     *
     * @return
     */
    public TextComponent getTimerMessage() {
        BaseComponent[] descComponent = (new ComponentBuilder(StringUtil.colorCode("&7" + gameEvent.getEventDescription()))).create();
        String middleMessage = " &cevent is &cstarting in &e" + StringUtil.getDurationString(timer);
        BaseComponent[] firstPart = new ComponentBuilder(StringUtil.colorCode("&8[&6&lWarlords&8] &cA "))
                .append(StringUtil.colorCode("&8[&2" + gameEvent.getEventName() + "&8]")).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, descComponent)).create();

        TextComponent queueClick = new TextComponent(StringUtil.colorCode("&8[&bClick here &bto &bqueue!&8] "));
        queueClick.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/event join"));
        TextComponent finalComponent = new TextComponent(firstPart);
        finalComponent.addExtra(StringUtil.colorCode(middleMessage));
        finalComponent.addExtra(queueClick);
        return finalComponent;
    }

    /**
     * Teleports players to their respected spawn points.
     */
    public void teleportPlayers() {
        List<Player> teleportList = new ArrayList<>();
        getPlayerList().forEach(player -> teleportList.add(player));

        new BukkitRunnable() {
            @Override
            public void run() {
                if (teleportList.isEmpty()) {
                    cancel();
                } else {
                    Player player = teleportList.get(0);
                    if (statusMechanic.getStatus(player) == StatusEnums.NONE) {
                        StatusEnums team = getLastStatus() == StatusEnums.BLUE ? StatusEnums.RED : StatusEnums.BLUE;
                        statusMechanic.setStatus(player, team);
                        setLastStatus(team);
                    }
                    switch (statusMechanic.getStatus(player)) {
                        case RED:
                            player.teleport(getActiveArena().getRedSpawn());
                            break;
                        case BLUE:
                            player.teleport(getActiveArena().getBlueSpawn());
                            break;
                        default:
                            player.teleport(getActiveArena().getBlueSpawn());
                            break;
                    }
                    teleportList.remove(0);
                }
            }
        }.runTaskTimer(WarlordsMain.getInstance(), 1, 1);

    }

    /**
     * @return
     */
    public GameEvent getGameEvent() {
        return gameEvent;
    }

    /**
     * used to check if game is active
     *
     * @return - active boolean
     */
    public boolean isActiveGame() {
        return activeGame;
    }

    /**
     * Starts the Game Instance
     */
    private void startGame(GameEvent gameEvent) {
        if (getTeamSize(StatusEnums.RED) <= 0 && getTeamSize(StatusEnums.BLUE) <= 0) {
            resetTimer();
            return;
        }
        setActive(true);
        gameEvent.init();
    }

    /**
     * Used to get List of Spectators
     *
     * @return - Returns Spectators
     */
    public ArrayList<Player> getSpectatorList() {
        return spectatorList;
    }

    /**
     * Adds spectator to List
     *
     * @param player
     */
    public void addSpectator(Player player) {
        spectatorList.add(player);
        player.getInventory().clear();
        player.setGameMode(GameMode.SPECTATOR);
    }

    /**
     * Removes Spectator
     *
     * @param player
     */
    public void removeSpectator(Player player) {

        player.setGameMode(GameMode.SURVIVAL);
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + player.getName());
        spectatorList.remove(player);
    }

    /**
     * @return - Returns the Active Arena
     */
    public Arena getActiveArena() {
        return activeArena;
    }

    /**
     * Sends a message to all active players
     *
     * @param message - Message to send
     */
    public void sendGameMessage(String message) {
        playerList.forEach(player -> player.sendMessage(message));
    }

    /**
     * Gets the list of Players
     *
     * @return - Returns all Players playing.
     */
    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    /**
     * Tell the plugin if a game is currently going on or not
     *
     * @param activeGame - True or False depending if a game is active.
     */
    public void setActive(boolean activeGame) {
        this.activeGame = activeGame;
    }

    /**
     * Ends the game correctly while resetting all info.
     */
    public void endGame() {
        playerList.forEach(player -> {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + player.getName());
            player.setGameMode(GameMode.SURVIVAL);
            player.getInventory().clear();
            player.setHealth(20);
            player.setFoodLevel(20);
            statusMechanic.setStatus(player, StatusEnums.NONE);
        });
        spectatorList.forEach(player -> {
            player.setGameMode(GameMode.SURVIVAL);
            player.setHealth(20);
            player.setFoodLevel(20);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + player.getName());
        });
        spectatorList.clear();
        playerList.clear();
        setActive(false);
        activeArena = null;
        resetTimer();
    }

    /**
     * Resets the timer so that games can start (This should never be run while a game is active)
     */
    public void resetTimer() {
        if (isActiveGame()) return;
        if (!EventConfig.get().isSet("gamemodes-wait-minutes")) {
            EventConfig.get().set("gamemodes-wait-minutes", 5);
            EventConfig.save();
        }
        timer = (EventConfig.get().getInt("gamemodes-wait-minutes") * 60);
        originalTime = timer;
    }

    /**
     * Removes a player from the game
     *
     * @param player - Player to remove
     */
    public void removePlayer(Player player) {
        if (!playerList.contains(player)) return;
        playerList.remove(player);
        statusMechanic.setStatus(player, StatusEnums.NONE);
    }

    /**
     * Location of the Lobby
     *
     * @return The Location of the Lobby
     */
    public Location getLobbyLocation() {
        return StringUtil.getLocationString(EventConfig.get().getString("gamemodes-lobby-location"));
    }


    /**
     * Adds a player to the game whilist balancing teams.
     *
     * @param player - Player to add
     */
    public void addPlayer(Player player) {
        if (isActiveGame()) {
            player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cThe event has already started! Please wait until it is over."));
            return;
        }
        if (playerList.contains(player)) {
            player.sendMessage("&8[&6&lWarlords&8] &cYou are already in the game!");
            return;
        }
        StatusEnums team = lastStatus == StatusEnums.BLUE ? StatusEnums.RED : StatusEnums.BLUE;
        statusMechanic.setStatus(player, team);
        player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cAdded to the " + team.getDisplayName() + "&cteam."));
        lastStatus = team;

        player.getInventory().clear();
        playerList.add(player);
        player.teleport(getLobbyLocation());
    }

    /**
     * Gets Total Size of all Playing War
     *
     * @return - Count
     */
    public int getTotalPlayers() {
        return playerList.size();
    }

    /**
     * Gathers all the players on one team.
     *
     * @param enums - Team to check for
     * @return - Returns list of Players
     */
    public List<Player> getTeamPlayers(StatusEnums enums) {
        List<Player> teamList = new ArrayList<>();
        for (Player player : playerList) {
            if (WarlordsMain.getMechanicHandler().getStatusMechanic().getStatus(player) == enums) teamList.add(player);
        }
        return teamList;
    }

    /**
     * Gets the team that won the game.
     *
     * @return The Team that won
     */
    public StatusEnums getWinningTeam() {
        if (getTeamSize(StatusEnums.RED) >= 1) {
            return StatusEnums.RED;
        } else {
            return StatusEnums.BLUE;
        }
    }

    /**
     * Gets the size of a Team
     *
     * @param enums - The Team/Status you're checking
     * @return - Returns Size
     */
    public int getTeamSize(StatusEnums enums) {
        int count = 0;
        for (Player player : playerList) {
            if (WarlordsMain.getMechanicHandler().getStatusMechanic().getStatus(player) == enums) count++;
        }
        return count;
    }
}
