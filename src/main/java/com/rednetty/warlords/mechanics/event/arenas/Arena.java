package com.rednetty.warlords.mechanics.event.arenas;

import org.bukkit.Location;
import org.jetbrains.annotations.Nullable;


public class Arena {
    private String arenaName;
    private Location redSpawn;
    private Location blueSpawn;

    public Arena(String arenaName, @Nullable Location redSpawn, @Nullable Location blueSpawn) {
        this.blueSpawn = blueSpawn;
        this.redSpawn = redSpawn;
        this.arenaName = arenaName;
    }

    /**
     * Checks if the Arena is completely setup
     *
     * @return
     */
    public boolean isSetup() {
        if (redSpawn == null) return false;
        return redSpawn != null;
    }

    /**
     * Returns the spawn for people with Blue Status
     *
     * @return
     */
    public Location getBlueSpawn() {
        return blueSpawn;
    }

    /**
     * Used to set the spawn for Blue Status
     *
     * @param blueSpawn
     */
    public void setBlueSpawn(Location blueSpawn) {
        this.blueSpawn = blueSpawn;
    }

    /**
     * Returns the spawn for people with Red Status
     *
     * @return
     */
    public Location getRedSpawn() {
        return redSpawn;
    }

    /**
     * Used to set the spawn for Red Status
     *
     * @param redSpawn
     */
    public void setRedSpawn(Location redSpawn) {
        this.redSpawn = redSpawn;
    }

    /**
     * @return - The name of the Arena to display
     */
    public String getArenaName() {
        return arenaName;
    }

    /**
     * Used to set the Arena's Name
     *
     * @param arenaName - name of said arena
     */
    public void setArenaName(String arenaName) {
        this.arenaName = arenaName;
    }
}
