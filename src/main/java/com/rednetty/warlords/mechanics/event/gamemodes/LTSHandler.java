package com.rednetty.warlords.mechanics.event.gamemodes;

import com.rednetty.warlords.WarlordsMain;
import com.rednetty.warlords.mechanics.event.GameEvent;
import com.rednetty.warlords.utils.string.StringUtil;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;


public class LTSHandler extends GameEvent implements Listener {
    public LTSHandler() {
        super("Last Team Standing", "A last man standing team game-mode.");
    }

    /**
     * Starts the War Gamemode
     */
    public void init() {

        getEventMechanic().teleportPlayers();

        /*Gives the player his kit and sends him a little message ;)*/
        getEventMechanic().getPlayerList().forEach(player -> {
            WarlordsMain.getTitleManagerAPI().sendSubtitle(player, StringUtil.colorCode("&7The last team standing wins.."), 0, 60, 0);
            getEventMechanic().giveKit(player);
            player.setGameMode(GameMode.SURVIVAL);
            player.setHealth(20);
            player.setFoodLevel(20);
            getEventMechanic().sendRemainingBar();
        });

        new BukkitRunnable() {
            @Override
            public void run() {
                getEventMechanic().sendRemainingBar();
            }
        }.runTaskTimerAsynchronously(WarlordsMain.getInstance(), 30L, 30L);
    }


    @EventHandler
    public void onDeath(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (player.getHealth() - event.getDamage() <= 0 && getEventMechanic().getPlayerList().contains(player) && getEventMechanic().isActiveGame()) {
                event.setDamage(0);
                player.setHealth(1);
                getEventMechanic().eliminatePlayer(player);
            }
        }
    }

}