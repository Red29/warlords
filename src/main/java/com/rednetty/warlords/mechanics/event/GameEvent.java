package com.rednetty.warlords.mechanics.event;

import com.rednetty.warlords.WarlordsMain;

public class GameEvent {
    private String eventName;
    private String eventDescription;
    private EventMechanic eventMechanic = WarlordsMain.getMechanicHandler().getEventMechanic();

    public GameEvent(String eventName, String eventDescription) {
        this.eventName = eventName;
        this.eventDescription = eventDescription;
    }

    /**
     * What to run when the game is started
     */
    public void init() {

    }


    /**
     * Description of the Event
     *
     * @return
     */
    public String getEventDescription() {
        return eventDescription;
    }

    /**
     * Gets the main EventMechanic class
     *
     * @return Returns EventMechanic.class
     */
    public EventMechanic getEventMechanic() {
        return eventMechanic;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    /**
     * Display name of the Event
     *
     * @return - Returns String Name
     */
    public String getEventName() {
        return eventName;
    }
}
