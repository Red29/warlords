package com.rednetty.warlords.mechanics.event.arenas;

import com.rednetty.warlords.configs.ArenaConfig;
import com.rednetty.warlords.utils.string.StringUtil;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ArenaHandler {
    public ArrayList<Arena> arenasList = new ArrayList<>();


    public void init() {
        ArenaConfig.setup();
        loadArenas();
    }

    public void close() {
        saveArenas();
    }

    public ArrayList<Arena> getArenasList() {
        return arenasList;
    }

    /**
     * Picks a random Arena out of all the Finished Arenas
     *
     * @return - Returns Arena
     */
    public Arena getRandomArena() {
        Random random = new Random();
        List<Arena> finishedArenas = new ArrayList<>();
        arenasList.forEach(arena -> {
            if (arena.isSetup()) {
                finishedArenas.add(arena);
            }
        });
        return finishedArenas.get(random.nextInt(finishedArenas.size()));
    }

    /**
     * Used to grab an Arena via its namee
     */
    public Arena nameToArena(String name) {
        for (Arena arena : arenasList) {
            if (arena.getArenaName().equalsIgnoreCase(name)) {
                return arena;
            }
        }
        return null;
    }

    /**
     * Used to create a new Arena and Register it
     * Returns True if Created and False if the name is already in use.
     */
    public boolean createArena(String name) {
        if (nameToArena(name) == null) {
            arenasList.add(new Arena(name, null, null));
            return true;
        }
        return false;
    }

    /**
     * Saves the arenas to a Config File
     */
    private void saveArenas() {
        ArenaConfig.clearConfig();
        arenasList.forEach(arena -> {
            if (arena.isSetup()) {
                String name = arena.getArenaName();
                String redSpawn = StringUtil.getStringLocation(arena.getRedSpawn());
                String blueSpawn = StringUtil.getStringLocation(arena.getBlueSpawn());
                ArenaConfig.get().set(name + ".Reds Spawn Location", redSpawn);
                ArenaConfig.get().set(name + ".Blues Spawn Location", blueSpawn);
            }
        });
        ArenaConfig.save();
    }


    /**
     * Loads arenas from Config File
     */
    public void loadArenas() {
        ArenaConfig.get().getKeys(false).forEach(name -> {
            String redSpawnSerialized = ArenaConfig.get().getString(name + ".Reds Spawn Location");
            String blueSpawnSerialized = ArenaConfig.get().getString(name + ".Blues Spawn Location");
            Location redSpawn = StringUtil.getLocationString(redSpawnSerialized);
            Location blueSpawn = StringUtil.getLocationString(blueSpawnSerialized);
            arenasList.add(new Arena(name, redSpawn, blueSpawn));
        });
    }


}
