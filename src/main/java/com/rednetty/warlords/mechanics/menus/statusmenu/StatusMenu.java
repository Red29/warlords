package com.rednetty.warlords.mechanics.menus.statusmenu;

import com.rednetty.warlords.WarlordsMain;
import com.rednetty.warlords.enums.status.StatusEnums;
import com.rednetty.warlords.mechanics.menus.Button;
import com.rednetty.warlords.mechanics.menus.Menu;
import com.rednetty.warlords.mechanics.menus.MenuPlayer;
import com.rednetty.warlords.utils.items.ItemBuilder;
import com.rednetty.warlords.utils.string.StringUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class StatusMenu extends Menu {


    public StatusMenu(MenuPlayer player, boolean forced) {
        super(InventoryType.HOPPER, "&c&lSTATUS SELECTOR");
        for (StatusEnums statusEnums : StatusEnums.values()) {
            if (canSee(statusEnums.getPerms(), player.getPlayer(), forced)) {
                ItemStack statusItem = new ItemBuilder(statusEnums.getMaterial()).setName(StringUtil.colorCode("&cStatus: " + statusEnums.getItemName() + statusEnums.getDisplayName())).build();
                newButton(new Button(this, emptySlot(), statusItem, true) {
                    @Override
                    protected void onClick(int slot, MenuPlayer player) {
                        WarlordsMain.getMechanicHandler().getStatusMechanic().setStatus(player.getPlayer(), statusEnums);
                        player.closeMenu(true);
                    }
                });
            }

        }
    }

    public boolean canSee(String perm, Player player, boolean forced) {
        if (WarlordsMain.getMechanicHandler().getEventMechanic().getPlayerList().contains(player)) return false;
        if (perm.equalsIgnoreCase("")) return true;
        if (player.hasPermission(perm)) return true;
        if (forced) return true;
        return player.isOp();

    }

    @Override
    public void onClose(MenuPlayer player) {
    }

    @Override
    public void onOpen(MenuPlayer player) {

    }
}
