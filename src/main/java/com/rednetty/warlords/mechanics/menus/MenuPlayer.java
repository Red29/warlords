package com.rednetty.warlords.mechanics.menus;

import org.bukkit.entity.Player;

public class MenuPlayer {
    private Menu openMenu;
    private Player player;

    public MenuPlayer(Player player) {
        this.player = player;
    }

    /**
     * Get the MenuPlayer's Player
     *
     * @return Player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Open a menu for the player
     *
     * @param openMenu The menu to open
     */
    public void openMenu(Menu openMenu) {
        if (viewingMenu())
            closeMenu(true);

        this.openMenu = openMenu;
        this.openMenu.getViewers().add(this);
        this.openMenu.onOpen(this);
        player.openInventory(openMenu.getInventory());
    }

    /**
     * Close the currently opened menu of the player
     */
    public void closeMenu(boolean closeBukkitInv) {
        if (!viewingMenu()) return;
        openMenu.getViewers().remove(this);
        openMenu.onClose(this);
        if (closeBukkitInv)
            player.closeInventory();
        openMenu = null;
    }

    /**
     * Get the currently opened menu of the player
     *
     * @return Menu
     */
    public Menu getOpenMenu() {
        return openMenu;
    }


    public boolean viewingMenu() {
        return openMenu != null;
    }
}