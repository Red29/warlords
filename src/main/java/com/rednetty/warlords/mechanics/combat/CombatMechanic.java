package com.rednetty.warlords.mechanics.combat;

import com.rednetty.warlords.mechanics.Mechanic;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.MobEffects;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Iterator;

public class CombatMechanic extends Mechanic implements Listener {

    @Override
    public void onEnable() {
        listener(this);
    }

    @Override
    public void onDisable() {

    }

    @EventHandler(
            ignoreCancelled = true,
            priority = EventPriority.NORMAL
    )
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
            Player damaged = (Player) e.getEntity();
            Player damager = (Player) e.getDamager();
            boolean blocking = false;
            if (damaged.isBlocking()) {
                blocking = true;
            }

            if (this.isCritical(damager)) {
                double damage = e.getDamage();
                double d = damage * 0.75D;
                e.setDamage(d);
            }

            if (blocking) {
                EntityDamageEvent event = new EntityDamageEvent(damaged, EntityDamageEvent.DamageCause.ENTITY_ATTACK, 2.0D);
                Bukkit.getPluginManager().callEvent(event);
                damaged.damage(event.getDamage());
                damaged.setVelocity(damager.getLocation().getDirection().multiply(0.21D));
                e.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        p.setMaximumNoDamageTicks(17);
        p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).addModifier(new AttributeModifier("generic.attackSpeed", 9.9999999E7D, AttributeModifier.Operation.ADD_NUMBER));
    }


    @EventHandler
    public void quit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        Iterator var3 = p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getModifiers().iterator();

        while (var3.hasNext()) {
            AttributeModifier a = (AttributeModifier) var3.next();
            if (a.getName().equals("generic.attackSpeed")) {
                p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).removeModifier(a);
            }
        }

    }

    public boolean isCritical(Player p) {
        EntityHuman eh = ((CraftPlayer) p).getHandle();
        return eh.fallDistance > 0.0F && !eh.onGround && !eh.m_() && !eh.isInWater() && !eh.hasEffect(MobEffects.BLINDNESS) && !eh.isPassenger() && !eh.isSprinting();
    }

}
