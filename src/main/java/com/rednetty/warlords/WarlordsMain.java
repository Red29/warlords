package com.rednetty.warlords;

import com.rednetty.warlords.commands.arena.ArenaCommand;
import com.rednetty.warlords.commands.event.EventCommand;
import com.rednetty.warlords.commands.status.SetStatusCommand;
import com.rednetty.warlords.commands.status.StatusCommand;
import com.rednetty.warlords.mechanics.MechanicHandler;
import io.puharesource.mc.titlemanager.api.v2.TitleManagerAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class WarlordsMain extends JavaPlugin {
    private static WarlordsMain warlordsMain;
    private static MechanicHandler mechanicHandler;
    private static TitleManagerAPI titleManagerAPI;

    public static WarlordsMain getInstance() {
        return warlordsMain;
    }

    /**
     * @return Title API
     */
    public static TitleManagerAPI getTitleManagerAPI() {
        return titleManagerAPI;
    }

    /**
     * @return Returns a instance of the MechanicHandler class so that it can be accessed from anywhere.
     */
    public static MechanicHandler getMechanicHandler() {
        return mechanicHandler;
    }

    public void onEnable() {
        //Loading Title API
        titleManagerAPI = (TitleManagerAPI) Bukkit.getServer().getPluginManager().getPlugin("TitleManager");


        warlordsMain = this;

        /*Registers and Initializes the Mechanic System*/
        mechanicHandler = new MechanicHandler();
        mechanicHandler.init();

        /*Registers Commands*/
        registerCommands();
    }

    public void onDisable() {
        mechanicHandler.stop();
    }


    public void registerCommands() {
        getCommand("Event").setExecutor(new EventCommand());
        getCommand("Arena").setExecutor(new ArenaCommand());
        getCommand("SetStatus").setExecutor(new SetStatusCommand());
        getCommand("Status").setExecutor(new StatusCommand());
    }
}
