package com.rednetty.warlords.utils.string;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;

/**
 * Created by Giovanni on 12-2-2017.
 */
public class StringUtil {
    private final static int CENTER_PX = 154;

    public static String colorCode(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static void sendCenteredMessage(LivingEntity livingEntity, String message) {
        if (message == null || message.equals("")) livingEntity.sendMessage("");
        message = ChatColor.translateAlternateColorCodes('&', message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for (char c : message.toCharArray()) {
            if (c == '§') {
                previousCode = true;
            } else if (previousCode) {
                previousCode = false;
                isBold = c == 'l' || c == 'L';
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while (compensated < toCompensate) {
            sb.append(" ");
            compensated += spaceLength;
        }
        livingEntity.sendMessage(sb.toString() + message);
    }

    /**
     * Converts a location to a serialized String
     *
     * @param l - Location to serialize
     * @return - Returns a String of a serialized String
     */
    static public String getStringLocation(final Location l) {
        if (l == null) {
            return "";
        }
        return l.getWorld().getName() + ":" + l.getBlockX() + ":" + l.getBlockY() + ":" + l.getBlockZ();
    }

    /**
     * Converts Serialized String to a Location
     *
     * @param s - String
     * @return - Returns a Location if possible
     */
    static public Location getLocationString(final String s) {
        if (s == null || s.trim() == "") {
            return null;
        }
        final String[] parts = s.split(":");
        if (parts.length == 4) {
            final World w = Bukkit.getServer().getWorld(parts[0]);
            final int x = Integer.parseInt(parts[1]);
            final int y = Integer.parseInt(parts[2]);
            final int z = Integer.parseInt(parts[3]);
            return new Location(w, x, y, z);
        }
        return null;
    }

    public static String getFullMessage(String[] args, int start) {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i < args.length; i++) {
            sb.append(args[i]).append(" ");
        }

        String allArgs = sb.toString().trim();
        return allArgs;
    }

    public static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    public static String getDurationString(int seconds) {
        String toReturn = "";
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        if (seconds > 0) {
            toReturn = seconds + "s ";
        }
        if (minutes > 0) {
            toReturn = minutes + "m " + toReturn;
        }
        if (hours > 0) {
            toReturn = hours + "h " + toReturn;
        }

        return toReturn;
    }

}