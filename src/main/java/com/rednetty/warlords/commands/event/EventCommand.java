package com.rednetty.warlords.commands.event;


import com.rednetty.warlords.WarlordsMain;
import com.rednetty.warlords.mechanics.event.EventMechanic;
import com.rednetty.warlords.utils.string.StringUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EventCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            EventMechanic eventMechanic = WarlordsMain.getMechanicHandler().getEventMechanic();
            if (strings.length == 1) {
                switch (strings[0].toLowerCase()) {
                    case "join":
                        if (eventMechanic.getPlayerList().contains(player)) {
                            player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cAlready in the game!"));
                            return true;
                        } else if (eventMechanic.getArenaHandler().arenasList.isEmpty()) {
                            player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cNo arenas are setup!"));
                            return true;
                        } else {
                            eventMechanic.addPlayer(player);
                        }
                        break;
                    case "leave":
                        if (eventMechanic.getPlayerList().contains(player)) {
                            if (eventMechanic.isActiveGame()) {
                                eventMechanic.eliminatePlayer(player);
                                return true;
                            } else {
                                eventMechanic.removePlayer(player);
                            }
                        } else if (eventMechanic.getSpectatorList().contains(player)) {
                            eventMechanic.removeSpectator(player);
                        } else {
                            player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cYou're not in any games!"));
                            return true;
                        }
                        break;
                    default:
                        player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cIncorrect Args /event <join/leave>"));
                        break;
                }
            }
            return false;
        } else {
            return true;
        }
    }
}
