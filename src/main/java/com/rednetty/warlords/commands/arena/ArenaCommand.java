package com.rednetty.warlords.commands.arena;

import com.rednetty.warlords.WarlordsMain;
import com.rednetty.warlords.configs.EventConfig;
import com.rednetty.warlords.mechanics.event.arenas.Arena;
import com.rednetty.warlords.mechanics.event.arenas.ArenaHandler;
import com.rednetty.warlords.utils.string.StringUtil;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ArenaCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player && commandSender.hasPermission("warlords.arena")) {
            ArenaHandler arenaHandler = WarlordsMain.getMechanicHandler().getEventMechanic().getArenaHandler();
            Player player = (Player) commandSender;
            /**
             * Help Command that Displays all sub command Options
             */
            if (strings.length == 0) {
                player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cTry using /arena help."));
            }
            if (strings.length == 1 && strings[0].equalsIgnoreCase("help")) {
                player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &c The following are sub commands to be used with the /arena command"));
                player.sendMessage(StringUtil.colorCode("&b/arena setlobby &7- Sets the Team-Event Lobby."));
                player.sendMessage(StringUtil.colorCode("&b/arena list &7- Lists all arenas."));
                player.sendMessage(StringUtil.colorCode("&b/arena create <name> &7- Creates an arena shell."));
                player.sendMessage(StringUtil.colorCode("&b/arena delete <arena> &7- Deletes an arena."));
                player.sendMessage(StringUtil.colorCode("&b/arena rename <arena> <new-name> &7- Renames an arena."));
                player.sendMessage(StringUtil.colorCode("&b/arena setspawn <arena> <red/blue> &7- Sets the spawn of an arena team."));
            }
            /**
             * Sets the Team Event Lobby at the location of the sender.
             */
            if (strings.length == 1 && strings[0].equalsIgnoreCase("setlobby")) {
                EventConfig.get().set("gamemodes-lobby-location", StringUtil.getStringLocation(player.getLocation()));
                EventConfig.save();
                player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cLobby set at your location"));
            }
            /**
             * Lists all Arenas to the player.
             */
            if (strings.length == 1 && strings[0].equalsIgnoreCase("list")) {
                arenaHandler.getArenasList().forEach(arena -> {
                    int index = arenaHandler.getArenasList().indexOf(arena);
                    String endOfMessage = ".";
                    if (!arena.isSetup()) {
                        endOfMessage = ". &c(NOT SETUP)";
                    }
                    player.sendMessage(StringUtil.colorCode("&7" + index + ". &b" + arena.getArenaName() + endOfMessage));
                });
            }
            /**
             * Creates an arena if one doesn't exist
             */
            if (strings.length == 2 && strings[0].equalsIgnoreCase("create")) {
                if (arenaHandler.createArena(strings[1])) {
                    player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &b" + strings[1] + " has been created."));
                } else {
                    player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cArena already exists!"));
                }

            }
            /**
             * Deleting Arenas from the code and files.
             */
            if (strings.length == 2 && strings[0].equalsIgnoreCase("delete")) {
                if (arenaHandler.nameToArena(strings[1]) != null) {
                    Arena arena = arenaHandler.nameToArena(strings[1]);
                    arenaHandler.getArenasList().remove(arena);
                    player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &c" + arena.getArenaName() + " has been deleted."));
                } else {
                    player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cIncorrect Args! (/arena delete <arena>)"));
                }
            }
            /**
             * Renaming Arenas
             */
            if (strings.length == 3 && strings[0].equalsIgnoreCase("rename")) {
                if (arenaHandler.nameToArena(strings[1]) != null) {
                    Arena arena = arenaHandler.nameToArena(strings[1]);
                    String oldName = arena.getArenaName();
                    arenaHandler.getArenasList().remove(arena);
                    arena.setArenaName(strings[2]);
                    arenaHandler.getArenasList().add(arena);
                    player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &c" + oldName + " has been renamed to " + arena.getArenaName() + "."));
                } else {
                    player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cIncorrect Args! (/arena rename <arena> <new-name>)"));
                }
            }
            /**
             * Setting Arena Spawns
             */
            if (strings.length == 3 && strings[0].equalsIgnoreCase("setspawn")) {
                if (arenaHandler.nameToArena(strings[1]) != null) {
                    boolean worked = false;
                    Location location = player.getLocation();
                    Arena arena = arenaHandler.nameToArena(strings[1]);
                    switch (strings[2].toLowerCase()) {
                        case "blue":
                            arenaHandler.getArenasList().remove(arena);
                            arena.setBlueSpawn(location);
                            arenaHandler.getArenasList().add(arena);
                            worked = true;
                            break;
                        case "red":
                            arenaHandler.getArenasList().remove(arena);
                            arena.setRedSpawn(location);
                            arenaHandler.getArenasList().add(arena);
                            worked = true;
                            break;
                        default:
                            player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cIncorrect Args! (/arena setspawn <arena> <red/blue>)"));
                            break;
                    }
                    if (worked) {
                        player.sendMessage(StringUtil.colorCode("&8[&6&lWarlords&8] &cTeam spawn set."));
                        return false;
                    } else {
                        return false;
                    }


                }
            }
        }
        return true;
    }
}
