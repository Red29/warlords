package com.rednetty.warlords.commands.status;

import com.rednetty.warlords.WarlordsMain;
import com.rednetty.warlords.mechanics.menus.MenuPlayer;
import com.rednetty.warlords.mechanics.menus.statusmenu.StatusMenu;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatusCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            MenuPlayer menuPlayer = WarlordsMain.getMechanicHandler().getMenuHandler().getMenuPlayer(player);
            menuPlayer.openMenu(new StatusMenu(menuPlayer, false));
            return false;
        } else {
            return true;
        }
    }
}
