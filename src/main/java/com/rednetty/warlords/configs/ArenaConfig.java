package com.rednetty.warlords.configs;

import com.rednetty.warlords.WarlordsMain;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ArenaConfig {
    static FileConfiguration gameFile;
    static File arenaFile;

    public static void setup() {
        arenaFile = new File(WarlordsMain.getInstance().getDataFolder(), "Arenas.yml");

        if (!arenaFile.exists()) {
            try {
                arenaFile.createNewFile();
            } catch (IOException e) {
            }
        }

        gameFile = YamlConfiguration.loadConfiguration(arenaFile);
    }

    public static FileConfiguration get() {
        return gameFile;
    }

    public static void clearConfig() {
        arenaFile.delete();
        setup();
    }

    public static void save() {
        try {
            gameFile.save(arenaFile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save Arenas.yml!");
        }
    }

    public static void reload() {
        gameFile = YamlConfiguration.loadConfiguration(arenaFile);
    }

}
