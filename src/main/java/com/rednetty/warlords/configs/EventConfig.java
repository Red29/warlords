package com.rednetty.warlords.configs;

import com.rednetty.warlords.WarlordsMain;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class EventConfig {
    static FileConfiguration gameFile;
    static File eventFile;

    public static void setup() {
        eventFile = new File(WarlordsMain.getInstance().getDataFolder(), "TeamEvent.yml");

        if (!eventFile.exists()) {
            try {
                eventFile.createNewFile();
            } catch (IOException e) {
            }
        }
        gameFile = YamlConfiguration.loadConfiguration(eventFile);
    }

    public static FileConfiguration get() {
        return gameFile;
    }

    public static void clearConfig() {
        eventFile.delete();
        setup();
    }

    public static void save() {
        try {
            gameFile.save(eventFile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save TeamEvent.yml!");
        }
    }

    public static void reload() {
        gameFile = YamlConfiguration.loadConfiguration(eventFile);
    }

}